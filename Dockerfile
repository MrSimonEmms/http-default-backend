ARG IMG="node:12-alpine"
FROM ${IMG} as builder
WORKDIR /opt/app
ADD . .
EXPOSE 3000
RUN npm i --production
CMD [ "npm", "start" ]
