# HTTP Default Backend

Default backend to redirect to a URL

## Environment Variables

- `HEALTH_CHECK_URL`: `<string>` - `(/healthz)`
- `HTTP_REDIRECT`: `<string>` - `(https://google.com)`
- `HTTP_REDIRECT_CODE`: `<number>` - `(302)`
- `SERVER_PORT`: `<number>` - `(3000)`
