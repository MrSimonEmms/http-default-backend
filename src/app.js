/**
 * app
 */

/* Node modules */

/* Third-party modules */
const express = require('express');

/* Files */
const config = require('./config');

const app = express();

console.log('Config', JSON.stringify(config));

app
  .set('x-powered-by', null)
  .use((req, res, next) => {
    console.log('Incoming HTTP call', JSON.stringify({
      url: req.url,
      method: req.method,
      query: req.query,
      params: req.params,
    }));

    next();
  })
  .all(config.healthCheck, (req, res) => {
    res.sendStatus(200);
  })
  .all('/*', (req, res) => {
    res.redirect(config.redirectCode, config.redirect);
  });

app.listen(config.port, () => {
  console.log(`Listening on ${config.port}`);
});
