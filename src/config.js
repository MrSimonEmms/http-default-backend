/**
 * config
 */

/* Node modules */

/* Third-party modules */

/* Files */

module.exports = {
  healthCheck: process.env.HEALTH_CHECK_URL || '/healthz',
  port: Number(process.env.SERVER_PORT || 3000),
  redirect: process.env.HTTP_REDIRECT || 'http://google.com',
  redirectCode: Number(process.env.HTTP_REDIRECT_CODE || 302),
};
